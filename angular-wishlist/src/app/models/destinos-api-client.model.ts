import { forwardRef, Inject, Injectable } from '@angular/core';
import { from } from 'rxjs';
import { DestinoViaje } from '../models/DestinoViaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG, MyDatabase, db } from './../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';


@Injectable()
export class DestinosApiClient {
  getById(id: string): DestinoViaje {
    throw new Error('Method not implemented.');
  }
  destinos: DestinoViaje[] = [];

  constructor( private store: Store<AppState>,
	@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
	private http: HttpClient) {
	}
	add(d:DestinoViaje){
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if (data.status === 200) {
				this.store.dispatch(new NuevoDestinoAction(d));
				const myDb = db;
				myDb.destinos.add(d);
				console.log('todos los destinos de la db!');
				myDb.destinos.toArray().then(destinos => console.log(destinos))
			  }
		});	
	}
	elegir(d:DestinoViaje){
		this.store.dispatch(new ElegidoFavoritoAction(d)); 	
	}

} 