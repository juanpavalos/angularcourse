import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from './../../models/DestinoViaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import {AppState} from './../../app.module';
import { ElegidoFavoritoAction , NuevoDestinoAction } from './../../models/destinos-viajes-state.model';
import { state } from '@angular/animations';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates:string[];
  all;
  
  constructor(
    public DestinosApiClient:DestinosApiClient,
    private store: Store<AppState>
    ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push('Se eligió: ' + d.nombre);
        }
      });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
    }
  
  ngOnInit(): void {
  }
	agregado (d: DestinoViaje) {
    this.DestinosApiClient.add(d);
    this.onItemAdded.emit(d);
		return false;
  }
  
  guardar(nombre:string, url:string):boolean {
  	let d = new DestinoViaje(nombre, url);
    this.DestinosApiClient.add(d);
    this.onItemAdded.emit(d);
    this.store.dispatch(new NuevoDestinoAction(d))
    return false;
  }
  
  elegido(d: DestinoViaje) {
    this.DestinosApiClient.elegir(d);
  }

  getAll() {

  }
}
